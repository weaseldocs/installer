# Welcome to Weasel
Weasel is small framework I created as part of a project for my thesis for a professional diploma in Systems Engineering.

## How to use it:
Just copy and paste the following in a terminal and follow through:
```bash
/bin/bash -c "$(curl https://gitlab.com/weaseldocs/installer/-/raw/main/script.sh)"
```
The file structure is very simple, and shouldn't pose any problems to use and build from. Enjoy!
