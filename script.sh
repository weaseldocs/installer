#!/bin/bash
CYAN='\033[0;36m'
RED='\033[0;31'
NC='\033[0m'
if ! command -v git &> /dev/null
then
    echo "Git not found. Install it or make sure it is in PATH"
    exit
fi
if ! command -v go &> /dev/null
then
    echo "Go not found. Install it or make sure it is in PATH"
    exit
fi
echo -e "${CYAN}Setting up weasel"
echo -e "Enter module name: ${NC}"
read -p "" name
mkdir $name && cd $name
echo ""
printf "${CYAN}The default is just the web service template, would you like to use a template with a client in React? [y/N]: ${NC}"
read -p "" react
react=${react:-N}
if [ $react = "y" ] || [ $react = "Y" ]
then
    git clone https://gitlab.com/weaseldocs/weasel-template-plus-front.git .
    cd service
else
    git clone https://gitlab.com/weaseldocs/weasel-template .
fi
go mod init $name
go get -u github.com/jackc/pgx/v4/pgxpool
go get -u github.com/gin-gonic/gin
go get -u github.com/gin-contrib/cors
go mod tidy
echo ""
echo -e "${CYAN}Setup completed!"
echo ""
if [ $react = "y" ] || [ $react = "Y" ]
then
    echo -e "${CYAN}cd into the client folder and install dependencies and set all configuration according to your project, such as the base URL for axios."
fi
echo -e "${CYAN}For the web service, set the environment variables and update the utils.go file, and change the main.go file to use the port of your preference."